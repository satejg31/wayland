#include <wayland-client.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "xdg-shell-protocol.h"

struct wl_compositor* compositor = NULL;
struct wl_surface* surface = NULL;
struct wl_buffer* buffer = NULL;
struct wl_shm* shared_memory = NULL;
struct xdg_wm_base* shell = NULL;
struct xdg_toplevel* toplevel = NULL;
struct wl_callback_listener callback_listener;

uint8_t* pixel = NULL;
uint16_t width = 1280;
uint16_t height = 800;
uint8_t close_flag = 0;

int32_t allocate_shared_memory(uint64_t size_in_bytes)
{
    char name[8];
    int32_t fd = -1;

    name[0] = '/';
    name[7] = '\0';

    for(uint8_t i = 1; i <6; i++)
        name[i] = (rand() & 23) + 97;

    fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, S_IWUSR | S_IRUSR | S_IWOTH | S_IROTH);
    if(fd == -1)
    {
        shm_unlink(name);
        fprintf(stderr, "Error : Not able to create shared memory");
        exit(EXIT_FAILURE);
    }

    shm_unlink(name);
    ftruncate(fd, size_in_bytes);

    return (fd);
}

void resize(void)
{
    int32_t fd;
    struct wl_shm_pool* pool = NULL;
    
    fd = allocate_shared_memory(height * width * 4);
    pixel = mmap(0, height * width * 4, 
                    PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if(pixel == NULL)
    {
        fprintf(stderr, "Error : Not able allocate for pixels");
        exit(EXIT_FAILURE);
    }

    pool = wl_shm_create_pool(shared_memory, fd, height * width * 4);
    if(pool == NULL)
    {
        fprintf(stderr, "Error : Not able to create shm pool");
        exit(EXIT_FAILURE);
    }

    buffer = wl_shm_pool_create_buffer(pool, 0, width, height, 
                                       width * 4, WL_SHM_FORMAT_ARGB8888);

    wl_shm_pool_destroy(pool);
    close(fd);
}

void draw_surface(void)
{
    memset(pixel, 255, width * height * 4);
    wl_surface_attach(surface, buffer, 0 , 0);
    wl_surface_damage_buffer(surface, 0, 0, width, height);
    wl_surface_commit(surface);
}

void new_frame(void* data, struct wl_callback* callback, uint32_t a)
{
    wl_callback_destroy(callback);
    callback = wl_surface_frame(surface);
    wl_callback_add_listener(callback, &callback_listener, NULL);
    draw_surface();
}

struct wl_callback_listener callback_listener = {
    .done = new_frame
};

void xdg_surface_configure(void* data, struct xdg_surface* xdg_surface, uint32_t serial_num)
{
    xdg_surface_ack_configure(xdg_surface, serial_num);
    if(!pixel)
    {
        resize();
    }

    draw_surface();
}

struct xdg_surface_listener xdg_listener = {
    .configure = xdg_surface_configure
};

void toplevel_configure(void* data, struct xdg_toplevel* toplevel, int32_t new_width, int32_t new_height, struct wl_array* stat)
{
    if(!new_width && !new_height)
        return;

    if(width != new_width || height != new_height)
    {
        munmap(pixel, width * height * 4);
        width = new_width;
        height = new_height;
        resize();
    }
}

void toplevel_close(void* data, struct xdg_toplevel* toplevel)
{
    close_flag = 1;
}

struct xdg_toplevel_listener toplevel_listener = {
    .configure = toplevel_configure,
    .close = toplevel_close
};

void shell_ping(void* data, struct xdg_wm_base* shell, uint32_t serial_num)
{
    xdg_wm_base_pong(shell, serial_num);
}

struct xdg_wm_base_listener shell_listener = {
    .ping = shell_ping
};

void global_registry(void* data, struct wl_registry* registry, uint32_t name, const char* intf, uint32_t version)
{
    if(!strcmp(intf, wl_compositor_interface.name))
    {
        compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 4);
    }
    else if(!strcmp(intf, wl_shm_interface.name))
    {
        shared_memory = wl_registry_bind(registry, name, &wl_shm_interface, 1);
    }
    else if(!strcmp(intf, xdg_wm_base_interface.name))
    {
        shell = wl_registry_bind(registry, name, &xdg_wm_base_interface, 1);
        xdg_wm_base_add_listener(shell, &shell_listener, NULL);
    }
}

void global_registry_remove(void* data, struct wl_registry* registry, uint32_t name)
{

}

struct wl_registry_listener registry_listener = {
    .global = global_registry,
    .global_remove = global_registry_remove
};

int main(int argc, char *argv[])
{
    struct wl_display *display = NULL;
    struct wl_registry* registry = NULL;
    struct xdg_surface* xdg_surface = NULL;
    struct wl_callback* callback = NULL;

    display = wl_display_connect(0);
    if(display == NULL)
    {
        fprintf(stderr, "Error : Not able to connect a display");
        exit(EXIT_FAILURE);
    }

    registry = wl_display_get_registry(display);
    if(registry == NULL)
    {
        fprintf(stderr, "Error : Not able to create registry structure");
        exit(EXIT_FAILURE);
    }

    wl_registry_add_listener(registry, &registry_listener, NULL);
    wl_display_roundtrip(display);

    surface = wl_compositor_create_surface(compositor);
    if(surface == NULL)
    {
        fprintf(stderr, "Error : Not able to create wayland surface");
        exit(EXIT_FAILURE);
    }

    callback = wl_surface_frame(surface);
    wl_callback_add_listener(callback, &callback_listener, NULL);

    xdg_surface = xdg_wm_base_get_xdg_surface(shell, surface);
    xdg_surface_add_listener(xdg_surface, &xdg_listener, NULL);

    toplevel = xdg_surface_get_toplevel(xdg_surface);
    xdg_toplevel_add_listener(toplevel, &toplevel_listener, NULL);
    xdg_toplevel_set_title(toplevel, "hello-wayland client");
    wl_surface_commit(surface);

    // Display loop
    while(wl_display_dispatch(display))
    {
        if(close_flag)
            break;
    }

    if(buffer)
        wl_buffer_destroy(buffer);

    xdg_toplevel_destroy(toplevel);
    xdg_surface_destroy(xdg_surface);
    wl_surface_destroy(surface);
    wl_display_disconnect(display);
    exit(EXIT_SUCCESS);
}
